import random
import sys
from AbstractGame import AbstractGame
from OptionValidator import OptionValidator
from LogDec import name_info


class TTTGame(AbstractGame):
    def __init__(self):
        self.board = [' '] * 9
        self.man = None
        self.machine = None
        self.boardNumbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8']
        self.validator = OptionValidator()
        self.winningConfigurations = ([0, 1, 2], [3, 4, 5], [6, 7, 8], [0, 3, 6],
                                      [1, 4, 7], [2, 5, 8], [0, 4, 8], [2, 4, 6])
        self.boardForm = '''
           \t| %s | %s | %s |
           \t-------------
           \t| %s | %s | %s |
           \t-------------
           \t| %s | %s | %s |
           '''

    @name_info
    def run_game(self):
        self.display_instructions()
        decide = self.choose_who_first()
        self.print_board()
        self.start_game(decide)
        self.play_again()

    @name_info
    def start_game(self, decide):
        running = True
        player = decide
        while running:
            if player == 'h':
                self.man_move(self.board)
                if self.check_if_win(self.board, self.man):
                    print("You win")
                    self.print_board()
                    running = False
                else:
                    if self.is_board_full(self.board):
                        self.print_board()
                        running = False
                    else:
                        self.print_board()
                        player = 'm'
            else:
                self.machine_move(self.board)
                if self.check_if_win(self.board, self.machine):
                    print('I win !!!')
                    self.print_board()
                    running = False
                else:
                    if self.is_board_full(self.board):
                        self.print_board()
                        running = False
                    else:
                        self.print_board()
                        player = "h"

    @name_info
    def display_instructions(self):
        print(
            """
        Welcome to  Tic-Tac-Toe.

        You will make your move known by entering a number, 0 - 8.
        The number will correspond to the board position as illustrated:

                       | 0 | 1 | 2 |
                        ---------
                       | 3 | 4 | 5 |
                        ---------
                       | 6 | 7 | 8 |

        """)

    @name_info
    def choose_who_first(self):
        answer = None
        while answer not in ['y', 'n']:
            answer = input("Do you want to go first?").lower()
            self.validator.validate_yes_no(answer)
            if answer == 'y':
                self.man, self.machine = 'O', 'X'
                print("You are O.")
                return "h"
            elif answer == 'n':
                self.man, self.machine = 'X', 'O'
                print("You are X.")
                return "m"

    @name_info
    def print_board(self, board=None):
        if board is None:
            print(self.boardForm % tuple(self.board[6:9] + self.board[3:6] + self.board[0:3]))
        else:
            print(self.boardForm % tuple(board[6:9] + board[3:6] + board[0:3]))

    @name_info
    def man_move(self, board):
        answer = input('What is your move? Choose the number: 0-8')
        self.validator.validate_number(answer)
        answer = int(answer)
        if answer not in self.available_moves(board):
            print("Sorry, the place is already taken")
            self.man_move(board)
        else:
            board[answer] = self.man

    @name_info
    def machine_move(self, board):
        move = True
        while move:
            answer = random.randrange(0, 9)
            if answer not in self.available_moves(board):
                continue
            else:
                board[answer] = self.machine
                move = False

    @name_info
    def available_moves(self, board):
        return [k for k, v in enumerate(board) if v == ' ']

    @name_info
    def check_if_win(self, board, marker):
        for combo in self.winningConfigurations:
            if (board[combo[0]] == board[combo[1]] == board[combo[2]] == marker):
                return True
        return False

    @name_info
    def is_board_full(self, board):
        if " " in board:
            return False
        else:
            print("All places are taken. ")
            return True

    @name_info
    def play_again(self):
        answer = None
        while answer not in ['y', 'n']:
            answer = input("Play again?").lower()
            if answer == 'y':
                self.__init__()
                self.start_game(self.choose_who_first())
            elif answer == 'n':
                self.quit_game()
            else:
                print('You should type y or n')
                sys.exit()

    @name_info
    def quit_game(self):
        self.print_board()
        print("Thanks for playing :)")
        sys.exit()


@name_info
def main():
    game = TTTGame()
    game.run_game()


if __name__ == "__main__":
    main()
