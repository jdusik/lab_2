import abc


class AbstractGame(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def run_game(self):
        pass

    @abc.abstractmethod
    def play_again(self):
        pass