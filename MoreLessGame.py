import random
from AbstractGame import AbstractGame
from OptionValidator import OptionValidator
import sys

class MoreLessGame(AbstractGame):
    def __init__(self):
        self.number=random.randrange(0,101)

    def run_game(self):
        pass

    def play_again(self):
        answer=None
        while answer not in ['y','n']:
            answer = input("Play again?").lower()
            if answer == 'y':
                self.__init__()
                self.run_game()
            elif answer == 'n':
                self.quit_game()


    def quit_game(self):
        print("Thanks for playing :)")
        sys.exit()