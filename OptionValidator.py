from AbstractOptionValidator import AbstractValidator
from LogDec import name_info


class NotAString(Exception):
    pass

class NotInProperRange(Exception):
    pass

class OptionValidator(AbstractValidator):
    def __init__(self):
        self.zakres=[str(i) for i in range(0,9)]

#    @Logger.name_info
 #   def validate(self):
#        if not self._isString():
 #           raise NotAString()
 #       if not self._file_exist():
#            raise NotSuchFile()

    @name_info
    def validate_yes_no(self,answer):
        if not self._isString(answer):
            raise NotAString()

    @name_info
    def validate_number(self,answer):
        if not self._isString(answer):
            raise NotAString()
        self.isinProperRange(answer)

    def _isString(self,answer):
        return isinstance(answer, str)

    def isinProperRange(self,answer):
        if answer not in self.zakres:
            raise NotInProperRange()

