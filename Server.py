import socket
from TTTGame import TTTGame


class Server:
    def __init__(self, address, port, data_size):
        self.data_size = data_size
        self._createTcpIpSocket()
        self._bindSocketToThePort(address, port)
        self.gra=TTTGame()

    def handle_connection(self):
        self.sock.listen(1)
        while True:
            connection, client_address = self.sock.accept()
            dane=None
            while dane!='33':
                data_sign1 = connection.recv(self.data_size)
                dane=data_sign1.decode('utf-8')
                dane_out=self.gra.run_game()
                if dane:
                    connection.send(dane_out.encode('utf-8'))
        connection.close()
        return data_sign1

    def _createTcpIpSocket(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def _bindSocketToThePort(self, address, port):
        server_address = (address, port)
        print('bind to %s port %s' % server_address)
        self.sock.bind(server_address)


if __name__ == "__main__":
    host = 'localhost'
    port = 3000
    data_size = 1024
    server = Server(host, port, data_size)
    server.handle_connection()
    print(server.handle_connection())
