import socket


class Client:
    def __init__(self, address, port, data_size):
        self.data_size = data_size
        self._createTcpIpSocket()
        self.connectToServer(address, port)

    def sendMsg(self, msg1):
        msg =msg1.encode("utf-8")
        print(msg1)
        self.sock.send(msg)
        user_msg=None
        while user_msg!='33':
            response = self.sock.recv(self.data_size)
            response.decode('utf-8')
            print('receive %s' % response)
            user_msg=input("podaj pole")
            user_msg.encode('utf-8')
            self.sock.send(user_msg)
        self.sock.close()



    def _createTcpIpSocket(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def connectToServer(self, address, port):
        server_address = (address, port)
        print('connecting to %s port %s' % server_address)
        self.sock.connect(server_address)


if __name__ == "__main__":
    host = 'localhost'
    port = 3000
    data_size = 1024
    client = Client(host, port, data_size)
    message1 = input('Kolko czy krzyzyk')
    client.sendMsg(message1)
